#include <stdio.h>
#include "fonctions.h"

int main(int argc, char* argv[]) {
    char cNomImgLue[250], cNomImgEcrite[250], cNomImgLue2[250], cNomFichier[250], cNomImgEcrite2[250], cNomImgEcrite3[250], cNomImgLue3[250];
    int nH, nW, nTaille;

    if (argc < 2) {
        printf("## Traitement\n0 Seuillage | 1 Histogramme | 2 Profil | 3 Erosion | 4 Dilatation\n5 Fermerture | 6 Ouverture | 7 Enchainement | 8 Difference\n9 Inverse | 10 Flou(Moyenneur) | 11 Gradient | 12 Hysteresis\n13 Gaussien | 14 RGB to Y | 15 EQM | 16 RGB to YCbCr\n17 YCbCr to RGB | 18 ModifY | 19 DDP | 20 Repartition\n21 Egalisation\nEnd\n");
        exit(1);
    }

    int traitement = atoi(argv[1]);
    if (traitement < 0 || traitement > 21) {
        printf("## Traitement inconnue\nEnd\n");
        exit(1);
    }

    bool needWriteOutImage = !(traitement == 1 || traitement == 2 || traitement == 15 || traitement == 16 || traitement == 19 || traitement == 20);
    bool needDataFile = (traitement == 1 || traitement == 2 || traitement == 19 || traitement == 20);
    bool needImageInter = (traitement == 5 || traitement == 6 || traitement == 12);
    bool need2ImageInter = (traitement == 7);
    bool need2ImgIn = (traitement == 8 || traitement == 15);
    bool need3ImgOut = (traitement == 16);
    bool need3ImgIn = (traitement == 17);

    if (needDataFile) {
        if (argc < 4) {
            printf("## ImgIn cNomFichier\nEnd\n");
            exit(1);
        }
        sscanf(argv[3],"%s",cNomFichier);
    }

    if (need2ImgIn) {
        if (traitement == 8) {
            if (argc < 5) {
                printf("## ImgIn ImgOut ImgIn2\nEnd\n");
                exit(1);
            }
            sscanf(argv[4],"%s",cNomImgLue2);
        }
        else if (traitement == 15) {
            if (argc < 4) {
                printf("## ImgIn ImgIn2\nEnd\n");
                exit(1);
            }
            sscanf(argv[3],"%s",cNomImgLue2);
        }
    }

    if (needWriteOutImage) {
        if (traitement == 17) {
            if (argc < 6) {
                printf("## ImgIn ImgIn2 ImgIn3 ImgOut\nEnd\n");
                exit(1);
            }
            sscanf(argv[5], "%s", cNomImgEcrite);
        }
        else {
            if (argc < 4) {
                printf("## ImgIn ImgOut\nEnd\n");
                exit(1);
            }
            sscanf(argv[3], "%s", cNomImgEcrite);
        }
    }

    if (need3ImgOut) {
        if (argc < 6) {
            printf("## ImgIn ImgOut ImgOut2 ImgOut3\nEnd\n");
            exit(1);
        }
        sscanf(argv[3],"%s",cNomImgEcrite);
        sscanf(argv[4],"%s",cNomImgEcrite2);
        sscanf(argv[5],"%s",cNomImgEcrite3);
    }

    if (need3ImgIn) {
        if (argc < 6) {
            printf("## ImgIn ImgIn2 ImgIn3 ImgOut SVP\nEnd\n");
            exit(1);
        }
        sscanf(argv[3],"%s",cNomImgLue2);
        sscanf(argv[4],"%s",cNomImgLue3);
    }

    sscanf(argv[2],"%s",cNomImgLue);

    char ext[4] = "ppm";
    bool color = strcmp(cNomImgLue + strlen(cNomImgLue) - 3, ext) == 0;

    if (color)  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    else        lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);

    nTaille = color ? nH * nW * 3 : nH * nW;

    OCTET *ImgIn, *ImgOut, *ImgInter, *ImgInter2, *ImgIn2, *ImgOut2, *ImgOut3, *ImgIn3;
    allocation_tableau(ImgIn, OCTET, nTaille);

    if (needWriteOutImage) {
        if (traitement == 17) {
            allocation_tableau(ImgOut, OCTET, nTaille*3);
        }
        else {
            allocation_tableau(ImgOut, OCTET, nTaille);
        }
    }

    if (needImageInter) {
        allocation_tableau(ImgInter, OCTET, nTaille);
    }

    if (need2ImageInter) {
        allocation_tableau(ImgInter, OCTET, nTaille);
        allocation_tableau(ImgInter2, OCTET, nTaille);
    }

    if (need3ImgOut) {
        allocation_tableau(ImgOut, OCTET, nTaille);
        allocation_tableau(ImgOut2, OCTET, nTaille);
        allocation_tableau(ImgOut3, OCTET, nTaille);
    }

    if (color)  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    else        lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    if (need2ImgIn) {
        allocation_tableau(ImgIn2, OCTET, nTaille);

        if (color)  lire_image_ppm(cNomImgLue2, ImgIn2, nH * nW);
        else        lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
    }

    if (need3ImgIn) {
        allocation_tableau(ImgIn2, OCTET, nTaille);

        if (color)  lire_image_ppm(cNomImgLue2, ImgIn2, nH * nW);
        else        lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

        allocation_tableau(ImgIn3, OCTET, nTaille);

        if (color)  lire_image_ppm(cNomImgLue3, ImgIn3, nH * nW);
        else        lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);
    }

    switch(color) {
        case 0: {
            printf("PGM\n");
            switch(traitement) {
                case 0: {
                    printf("Seuillage\n");
                    seuil_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 1: {
                    printf("Histogramme\n");
                    histo_pgm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 2: {
                    printf("Profil\n");
                    profil_pgm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 3: {
                    printf("Erosion\n");
                    erosion_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 4: {
                    printf("Dilatation\n");
                    dilatation_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 5: {
                    printf("Fermeture\n");
                    fermeture_pgm(nW,nH,ImgIn,ImgOut,ImgInter);
                    break;
                }
                case 6: {
                    printf("Ouverture\n");
                    ouverture_pgm(nW,nH,ImgIn,ImgOut,ImgInter);
                    break;
                }
                case 7: {
                    printf("Enchainement Fermeture Ouverture\n");
                    enchainement_fer_ouv_pgm(nW,nH,ImgIn,ImgOut,ImgInter, ImgInter2);
                    break;
                }
                case 8: {
                    printf("Difference\n");
                    difference_pgm(nW,nH,ImgIn,ImgIn2,ImgOut);
                    break;
                }
                case 9: {
                    printf("Inverse\n");
                    inverse_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 10: {
                    printf("Flou\n");
                    flou_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 11: {
                    printf("Gradient\n");
                    norme_gradient_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 12: {
                    printf("Seuillage hysteresis\n");
                    seuil_hysteresis_pgm(nW,nH,ImgIn,ImgOut,ImgInter);
                    break;
                }
                case 13: {
                    printf("Filtre Gaussien\n");
                    gaussien_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 15: {
                    printf("EQM\n");
                    printf("%f\n",EQM_pgm(nW,nH,ImgIn,ImgIn2));
                    break;
                }
                case 17: {
                    printf("YCbCr to RGB\n");
                    YCbCrtoRGB(nW,nH,ImgIn,ImgIn2,ImgIn3,ImgOut);
                    break;
                }
                case 18: {
                    printf("ModifY\n");
                    modifY(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 19: {
                    printf("DDP\n");
                    ddp_pgm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 20: {
                    printf("Repartition\n");
                    repartition_pgm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 21: {
                    printf("Egalisation\n");
                    egalisation_pgm(nW,nH,ImgIn,ImgOut);
                    break;
                }
            }
            break;
        }
        case 1: {
            printf("PPM\n");
            switch(traitement) {
                case 0: {
                    printf("Seuillage\n");
                    seuil_ppm(nW, nH, ImgIn, ImgOut);
                    break;
                }
                case 1: {
                    printf("Histogramme\n");
                    histo_ppm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 2: {
                    printf("Profil\n");
                    profil_ppm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 9: {
                    printf("Inverse\n");
                    inverse_ppm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 10: {
                    printf("Flou\n");
                    flou_ppm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 13: {
                    printf("Filtre Gaussien\n");
                    gaussien_ppm(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 14: {
                    printf("RGB to Y\n");
                    RGBToY(nW,nH,ImgIn,ImgOut);
                    break;
                }
                case 15: {
                    printf("EQM\n");
                    printf("%f\n",EQM_ppm(nW,nH,ImgIn,ImgIn2));
                    break;
                }
                case 16: {
                    printf("RGB to YCbCr\n");
                    RGBtoYCbCr(nW,nH,ImgIn,ImgOut,ImgOut2,ImgOut3);
                    break;
                }
                case 19: {
                    printf("DDP\n");
                    ddp_ppm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 20: {
                    printf("Repartition\n");
                    repartition_ppm(nW,nH,ImgIn,cNomFichier);
                    break;
                }
                case 21: {
                    printf("Egalisation\n");
                    egalisation_ppm(nW,nH,ImgIn,ImgOut);
                    break;
                }
            }
            break;
        }
    }

    if (needWriteOutImage) {
        if (color && traitement != 14) ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
        else if (!color && traitement == 17) ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
        else ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
        printf("Image ecrite\n");
        free(ImgOut);
    }

    if (needImageInter) {
        free(ImgInter);
    }

    if (need2ImageInter) {
        free(ImgInter);
        free(ImgInter2);
    }

    if (need3ImgOut) {
        if (color && traitement != 16) {
            ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
            ecrire_image_ppm(cNomImgEcrite2, ImgOut2, nH, nW);
            ecrire_image_ppm(cNomImgEcrite3, ImgOut3, nH, nW);
        }
        else {
            ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
            ecrire_image_pgm(cNomImgEcrite2, ImgOut2, nH, nW);
            ecrire_image_pgm(cNomImgEcrite3, ImgOut3, nH, nW);
        }
        printf("Images ecrite\n");
        free(ImgOut);
        free(ImgOut2);
        free(ImgOut3);
    }

    if (needDataFile) {
        printf("Fichier ecrit\n");
    }


    free(ImgIn);

    if (need2ImgIn) {
        free(ImgIn2);
    }

    if (need3ImgIn) {
        free(ImgIn2);
        free(ImgIn3);
    }

    printf("End\n");
    return 0;
}
