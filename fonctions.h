#include <stdio.h>
#include "image_ppm.h" // manip image
#include <fstream> // manip fichier
#include <stdlib.h> // abs

using namespace std;

int indicePixel(int x, int y, int nW, int nH) {
    y = min(max(y, 0), nH - 1);
    x = min(max(x, 0), nW - 1);
    return y * nW + x;
}


void seuil_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    int nbSeuil;
    printf("Nb seuils: ");
    scanf("%d", &nbSeuil);

    if (nbSeuil < 1) {
        printf("## Au moin 1 seuil.\n");
        return;
    }

    int seuils[nbSeuil];

    for(int i = 0; i < nbSeuil; i++) {
        printf("Seuil %d: ", i + 1);
        scanf("%d", &seuils[i]);
    }

    for(int y = 0; y < nH; y++) {
        for(int x = 0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x, y, nW, nH)];
            ImgOut[indicePixel(x, y, nW, nH)] = 0;

            if (nbSeuil == 1) {
                if (valPixel >= seuils[0]) {
                    ImgOut[indicePixel(x, y, nW, nH)] = 255;
                }
            } else {
                for(int i = 0; i < nbSeuil; i++) {
                    if(valPixel >= seuils[i]) {
                        ImgOut[indicePixel(x, y, nW, nH)] = i * (255 / (nbSeuil - 1));
                    }
                }
            }
        }
    }
}


void histo_pgm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    int nbOcc[256];
    for(int i=0; i<256; i++) {
        nbOcc[i]=0;
    }

    for(int y=0; y<nH; y++) {
        for(int x=0; x<nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x,y,nW,nH)];
            nbOcc[valPixel]++;
        }
    }

    ofstream file(nom_fichier, ios::out | ios::binary);
    for(int i=0; i<256; i++){
        file << i << ' ' << nbOcc[i] << endl;
    }
    file.close();

    printf("plot \"%s\" with lines\n", nom_fichier);
}

void profil_pgm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    int ligneOuColone, numero;
    printf("ligne=0 ou colone=1: ");
    scanf("%d", &ligneOuColone);
    printf("n°?: ");
    scanf("%d", &numero);

    if(ligneOuColone==0) {
        int nivGris[nW];

        for(int x=0; x<nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x,numero,nW,nH)];
            nivGris[x]=valPixel;
        }

        ofstream file(nom_fichier, ios::out | ios::binary);
        for(int i=0; i<nW; i++) {
            file << i << ' ' << nivGris[i] << endl;
        }
        file.close();
    }
    else {
        int nivGris[nW];

        for(int y=0; y<nH; y++) {
            OCTET valPixel = ImgIn[indicePixel(numero,y,nW,nH)];
            nivGris[y]=valPixel;
        }

        ofstream file(nom_fichier, ios::out | ios::binary);
        for(int i=0; i<nW; i++) {
            file << i << ' ' << nivGris[i] << endl;
        }
        file.close();
    }
    printf("plot \"%s\" with lines\n", nom_fichier);
}

void profil_ppm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    int ligneOuColone, numero;
    printf("ligne=0 ou colone=1: ");
    scanf("%d", &ligneOuColone);
    printf("n°?: ");
    scanf("%d", &numero);

    if(ligneOuColone==0) {
        int nivR[nW];
        int nivG[nW];
        int nivB[nW];

        for(int x=0; x<nW; x++) {
            OCTET valR = ImgIn[indicePixel(x,numero,nW,nH)*3];
            OCTET valG = ImgIn[indicePixel(x,numero,nW,nH)*3+1];
            OCTET valB = ImgIn[indicePixel(x,numero,nW,nH)*3+2];

            nivR[x]=valR;
            nivG[x]=valG;
            nivB[x]=valB;
        }

        ofstream file(nom_fichier, ios::out | ios::binary);
        for(int i=0; i<nW; i++) {
            file << i << ' ' << nivR[i] << ' ' << nivG[i] << ' ' << nivB[i] << endl;
        }
        file.close();
    }
    else {
        int nivR[nW];
        int nivG[nW];
        int nivB[nW];

        for(int y=0; y<nH; y++) {
            OCTET valR = ImgIn[indicePixel(numero,y,nW,nH)*3];
            OCTET valG = ImgIn[indicePixel(numero,y,nW,nH)*3+1];
            OCTET valB = ImgIn[indicePixel(numero,y,nW,nH)*3+2];

            nivR[y]=valR;
            nivG[y]=valG;
            nivB[y]=valB;
        }

        ofstream file(nom_fichier, ios::out | ios::binary);
        for(int i=0; i<nW; i++) {
            file << i << ' ' << nivR[i] << ' ' << nivG[i] << ' ' << nivB[i] << endl;
        }
        file.close();
    }
    printf("plot \"%s\" using 1:2 with lines lc \"red\" title \"Red\", \"%s\" using 1:3 with lines lc \"green\" title \"Green\", \"%s\" using 1:4 with lines lc \"blue\" title \"Blue\"\n", nom_fichier, nom_fichier, nom_fichier);
}

void seuil_ppm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    int thresholds[3];
    printf("Seuil Red: ");
    scanf("%d", &thresholds[0]);
    printf("Seuil Green: ");
    scanf("%d", &thresholds[1]);
    printf("Seuil Blue: ");
    scanf("%d", &thresholds[2]);

    for(int y = 0; y < nH; y++) {
        for(int x = 0; x < nW; x++) {
            for(int c = 0; c < 3; c++) {
                OCTET valPixel = ImgIn[indicePixel(x, y, nW, nH) * 3 + c];
                OCTET threshold = thresholds[c];

                if (valPixel < threshold) {
                    ImgOut[indicePixel(x, y, nW, nH) * 3 + c] = 0;
                } else {
                    ImgOut[indicePixel(x, y, nW, nH) * 3 + c] = 255;
                }
            }
        }
    }
}

void histo_ppm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    int nbOccR[256];
    int nbOccG[256];
    int nbOccB[256];

    for(int i=0; i<256; i++) {
        nbOccR[i]=0;
        nbOccG[i]=0;
        nbOccB[i]=0;
    }

    for(int y=0; y<nH; y++) {
        for(int x=0; x<nW; x++) {
            OCTET valRPixel = ImgIn[indicePixel(x,y,nW,nH)*3];
            OCTET valGPixel = ImgIn[indicePixel(x,y,nW,nH)*3+1];
            OCTET valBPixel = ImgIn[indicePixel(x,y,nW,nH)*3+2];

            nbOccR[valRPixel]++;
            nbOccG[valGPixel]++;
            nbOccB[valBPixel]++;
        }
    }

    ofstream file(nom_fichier, ios::out | ios::binary);
    for(int i=0; i<256; i++){
        file << i << ' ' << nbOccR[i] << ' ' << nbOccG[i] << ' ' << nbOccB[i] << endl;
    }
    file.close();

    printf("plot \"%s\" using 1:2 with lines lc \"red\" title \"Red\", \"%s\" using 1:3 with lines lc \"green\" title \"Green\", \"%s\" using 1:4 with lines lc \"blue\" title \"Blue\"\n", nom_fichier, nom_fichier, nom_fichier);
}

void erosion_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y=0; y < nH; y++) {
        for (int x=0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x,y,nW,nH)];
            OCTET voisinD = ImgIn[indicePixel(x+1,y,nW,nH)];
            OCTET voisinG = ImgIn[indicePixel(x-1,y,nW,nH)];
            OCTET voisinB = ImgIn[indicePixel(x,y+1,nW,nH)];
            OCTET voisinH = ImgIn[indicePixel(x,y-1,nW,nH)];

            if(valPixel==0 || voisinD==0 || voisinG==0 || voisinB==0 || voisinH==0){
                ImgOut[indicePixel(x,y,nW,nH)]=0;
            }
            else {
                ImgOut[indicePixel(x,y,nW,nH)]=255;
            }
        }
    }
}

void dilatation_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y=0; y < nH; y++) {
        for (int x=0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x,y,nW,nH)];
            OCTET voisinD = ImgIn[indicePixel(x+1,y,nW,nH)];
            OCTET voisinG = ImgIn[indicePixel(x-1,y,nW,nH)];
            OCTET voisinB = ImgIn[indicePixel(x,y+1,nW,nH)];
            OCTET voisinH = ImgIn[indicePixel(x,y-1,nW,nH)];

            if(valPixel==255 || voisinD==255 || voisinG==255 || voisinB==255 || voisinH==255){
                ImgOut[indicePixel(x,y,nW,nH)]=255;
            }
            else {
                ImgOut[indicePixel(x,y,nW,nH)]=0;
            }
        }
    }
}

void fermeture_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgInter) {
    dilatation_pgm(nW,nH,ImgIn,ImgInter);
    erosion_pgm(nW,nH,ImgInter,ImgOut);
}

void ouverture_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgInter) {
    erosion_pgm(nW,nH,ImgIn,ImgInter);
    dilatation_pgm(nW,nH,ImgInter,ImgOut);
}

void enchainement_fer_ouv_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgInter, OCTET* ImgInter2) {
    int nbEnchainement;
    printf("n°?: ");
    scanf("%d", &nbEnchainement);

    fermeture_pgm(nW,nH,ImgIn,ImgInter,ImgInter2);
    for (int i=0; i < nbEnchainement-1; i++) {
        ouverture_pgm(nW,nH,ImgInter,ImgOut,ImgInter2);
        fermeture_pgm(nW,nH,ImgOut,ImgInter,ImgInter2);
    }
    ouverture_pgm(nW,nH,ImgInter,ImgOut,ImgInter2);
}

void difference_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgIn2, OCTET* ImgOut) {
    for (int y=0; y < nH; y++) {
        for (int x=0; x < nW; x++) {
            OCTET valPixelSeuil = ImgIn[indicePixel(x,y,nW,nH)];
            OCTET valPixelDil = ImgIn2[indicePixel(x,y,nW,nH)];

            if(valPixelSeuil==valPixelDil) {
                ImgOut[indicePixel(x,y,nW,nH)]=0;
            }
            else {
                ImgOut[indicePixel(x,y,nW,nH)]=255;
            }
        }
    }
}

void inverse_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y=0; y < nH; y++) {
        for (int x=0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x,y,nW,nH)];
            ImgOut[indicePixel(x,y,nW,nH)] = abs(valPixel-255);
        }
    }
}

void inverse_ppm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            int index = indicePixel(x, y, nW, nH) * 3;
            OCTET valPixel_R = ImgIn[index];
            OCTET valPixel_G = ImgIn[index + 1];
            OCTET valPixel_B = ImgIn[index + 2];

            ImgOut[index] = abs(valPixel_R - 255);
            ImgOut[index + 1] = abs(valPixel_G - 255);
            ImgOut[index + 2] = abs(valPixel_B - 255);
        }
    }
}

void flou_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    int demiTailleFiltre;
    printf("Demi Taille Filtre: ");
    scanf("%d", &demiTailleFiltre);

    for (int y=0; y < nH; y++) {
        for (int x=0; x < nW; x++) {
            int somme = 0;

            for (int dy=-demiTailleFiltre; dy<=demiTailleFiltre; dy++) {
                for (int dx=-demiTailleFiltre; dx<=demiTailleFiltre; dx++) {
                    somme += ImgIn[indicePixel(y+dy,x+dx,nW,nH)];
                }
            }
            ImgOut[indicePixel(y,x,nW,nH)] = somme / ((demiTailleFiltre*2+1)*(demiTailleFiltre*2+1));
        }
    }
}

void flou_ppm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    int demiTailleFiltre;
    printf("Demi Taille Filtre: ");
    scanf("%d", &demiTailleFiltre);

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            int sommeR = 0, sommeG = 0, sommeB = 0;
            int count = 0;

            for (int dy = -demiTailleFiltre; dy <= demiTailleFiltre; dy++) {
                for (int dx = -demiTailleFiltre; dx <= demiTailleFiltre; dx++) {
                    if (x + dx >= 0 && x + dx < nW && y + dy >= 0 && y + dy < nH) {
                        int index = indicePixel(y + dy, x + dx, nW, nH) * 3;
                        sommeR += ImgIn[index];
                        sommeG += ImgIn[index + 1];
                        sommeB += ImgIn[index + 2];
                        count++;
                    }
                }
            }

            int currentIndex = indicePixel(y, x, nW, nH) * 3;
            ImgOut[currentIndex] = sommeR / count;
            ImgOut[currentIndex + 1] = sommeG / count;
            ImgOut[currentIndex + 2] = sommeB / count;
        }
    }
}

void norme_gradient_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y=0; y < nH; y++) {
        for (int x=0; x < nW; x++) {
            OCTET monPixel = ImgIn[indicePixel(x,y,nW,nH)];
            OCTET voisinD = ImgIn[indicePixel(x+1,y,nW,nH)];
            OCTET voisinB = ImgIn[indicePixel(x,y+1,nW,nH)];

            int gradientX = voisinD-monPixel;
            int gradientY = voisinB-monPixel;

            ImgOut[indicePixel(x,y,nW,nH)]=sqrt(pow(gradientX,2)+pow(gradientY,2));
        }
    }
}

void seuil_hysteresis_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgInter) {
    int seuilHaut, seuilBas;
    printf("Seuil bas: ");
    scanf("%d", &seuilBas);
    printf("Seuil haut: ");
    scanf("%d", &seuilHaut);

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET monPixel = ImgIn[indicePixel(x, y, nW, nH)];
            if (monPixel <= seuilBas) {
                ImgOut[indicePixel(x, y, nW, nH)] = 0;
            } else if (monPixel >= seuilHaut) {
                ImgOut[indicePixel(x, y, nW, nH)] = 255;
            } else {
                ImgOut[indicePixel(x, y, nW, nH)] = 127;
            }
        }
    }

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET monPixel = ImgInter[indicePixel(x, y, nW, nH)];
            if (monPixel != 127) {
                continue;
            }

            if (monPixel >= seuilBas && monPixel <= seuilHaut) {
                for (int dy = -1; dy <= 1; dy++) {
                    for (int dx = -1; dx <= 1; dx++) {
                        if (ImgInter[indicePixel(x + dx, y + dy, nW, nH)] == 255) {
                            ImgOut[indicePixel(x, y, nW, nH)] = 255;
                            break;
                        }
                    }
                }
            } else {
                ImgOut[indicePixel(x, y, nW, nH)] = 0;
            }
        }
    }
}


void gaussien_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y = 2; y < nH - 2; y++) {
        for (int x = 2; x < nW - 2; x++) {
            int sum = 0;
            sum += 1 * ImgIn[indicePixel(x-2, y-2, nW, nH)];
            sum += 4 * ImgIn[indicePixel(x-1, y-2, nW, nH)];
            sum += 7 * ImgIn[indicePixel(x, y-2, nW, nH)];
            sum += 4 * ImgIn[indicePixel(x+1, y-2, nW, nH)];
            sum += 1 * ImgIn[indicePixel(x+2, y-2, nW, nH)];

            sum += 4 * ImgIn[indicePixel(x-2, y-1, nW, nH)];
            sum += 16 * ImgIn[indicePixel(x-1, y-1, nW, nH)];
            sum += 26 * ImgIn[indicePixel(x, y-1, nW, nH)];
            sum += 16 * ImgIn[indicePixel(x+1, y-1, nW, nH)];
            sum += 4 * ImgIn[indicePixel(x+2, y-1, nW, nH)];

            sum += 7 * ImgIn[indicePixel(x-2, y, nW, nH)];
            sum += 26 * ImgIn[indicePixel(x-1, y, nW, nH)];
            sum += 41 * ImgIn[indicePixel(x, y, nW, nH)];
            sum += 26 * ImgIn[indicePixel(x+1, y, nW, nH)];
            sum += 7 * ImgIn[indicePixel(x+2, y, nW, nH)];

            sum += 4 * ImgIn[indicePixel(x-2, y+1, nW, nH)];
            sum += 16 * ImgIn[indicePixel(x-1, y+1, nW, nH)];
            sum += 26 * ImgIn[indicePixel(x, y+1, nW, nH)];
            sum += 16 * ImgIn[indicePixel(x+1, y+1, nW, nH)];
            sum += 4 * ImgIn[indicePixel(x+2, y+1, nW, nH)];

            sum += 1 * ImgIn[indicePixel(x-2, y+2, nW, nH)];
            sum += 4 * ImgIn[indicePixel(x-1, y+2, nW, nH)];
            sum += 7 * ImgIn[indicePixel(x, y+2, nW, nH)];
            sum += 4 * ImgIn[indicePixel(x+1, y+2, nW, nH)];
            sum += 1 * ImgIn[indicePixel(x+2, y+2, nW, nH)];

            ImgOut[indicePixel(x, y, nW, nH)] = (OCTET)(sum / 273);
        }
    }
}

void gaussien_ppm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    for (int y = 2; y < nH - 2; y++) {
        for (int x = 2; x < nW - 2; x++) {
            int sumR = 0, sumG = 0, sumB = 0;

            sumR += 1 * ImgIn[indicePixel(x-2, y-2, nW, nH) * 3];
            sumR += 4 * ImgIn[indicePixel(x-1, y-2, nW, nH) * 3];
            sumR += 7 * ImgIn[indicePixel(x, y-2, nW, nH) * 3];
            sumR += 4 * ImgIn[indicePixel(x+1, y-2, nW, nH) * 3];
            sumR += 1 * ImgIn[indicePixel(x+2, y-2, nW, nH) * 3];

            sumR += 4 * ImgIn[indicePixel(x-2, y-1, nW, nH) * 3];
            sumR += 16 * ImgIn[indicePixel(x-1, y-1, nW, nH) * 3];
            sumR += 26 * ImgIn[indicePixel(x, y-1, nW, nH) * 3];
            sumR += 16 * ImgIn[indicePixel(x+1, y-1, nW, nH) * 3];
            sumR += 4 * ImgIn[indicePixel(x+2, y-1, nW, nH) * 3];

            sumR += 7 * ImgIn[indicePixel(x-2, y, nW, nH) * 3];
            sumR += 26 * ImgIn[indicePixel(x-1, y, nW, nH) * 3];
            sumR += 41 * ImgIn[indicePixel(x, y, nW, nH) * 3];
            sumR += 26 * ImgIn[indicePixel(x+1, y, nW, nH) * 3];
            sumR += 7 * ImgIn[indicePixel(x+2, y, nW, nH) * 3];

            sumR += 4 * ImgIn[indicePixel(x-2, y+1, nW, nH) * 3];
            sumR += 16 * ImgIn[indicePixel(x-1, y+1, nW, nH) * 3];
            sumR += 26 * ImgIn[indicePixel(x, y+1, nW, nH) * 3];
            sumR += 16 * ImgIn[indicePixel(x+1, y+1, nW, nH) * 3];
            sumR += 4 * ImgIn[indicePixel(x+2, y+1, nW, nH) * 3];

            sumR += 1 * ImgIn[indicePixel(x-2, y+2, nW, nH) * 3];
            sumR += 4 * ImgIn[indicePixel(x-1, y+2, nW, nH) * 3];
            sumR += 7 * ImgIn[indicePixel(x, y+2, nW, nH) * 3];
            sumR += 4 * ImgIn[indicePixel(x+1, y+2, nW, nH) * 3];
            sumR += 1 * ImgIn[indicePixel(x+2, y+2, nW, nH) * 3];

            sumG += 1 * ImgIn[indicePixel(x-2, y-2, nW, nH) * 3 + 1];
            sumG += 4 * ImgIn[indicePixel(x-1, y-2, nW, nH) * 3 + 1];
            sumG += 7 * ImgIn[indicePixel(x, y-2, nW, nH) * 3 + 1];
            sumG += 4 * ImgIn[indicePixel(x+1, y-2, nW, nH) * 3 + 1];
            sumG += 1 * ImgIn[indicePixel(x+2, y-2, nW, nH) * 3 + 1];

            sumG += 4 * ImgIn[indicePixel(x-2, y-1, nW, nH) * 3 + 1];
            sumG += 16 * ImgIn[indicePixel(x-1, y-1, nW, nH) * 3 + 1];
            sumG += 26 * ImgIn[indicePixel(x, y-1, nW, nH) * 3 + 1];
            sumG += 16 * ImgIn[indicePixel(x+1, y-1, nW, nH) * 3 + 1];
            sumG += 4 * ImgIn[indicePixel(x+2, y-1, nW, nH) * 3 + 1];

            sumG += 7 * ImgIn[indicePixel(x-2, y, nW, nH) * 3 + 1];
            sumG += 26 * ImgIn[indicePixel(x-1, y, nW, nH) * 3 + 1];
            sumG += 41 * ImgIn[indicePixel(x, y, nW, nH) * 3 + 1];
            sumG += 26 * ImgIn[indicePixel(x+1, y, nW, nH) * 3 + 1];
            sumG += 7 * ImgIn[indicePixel(x+2, y, nW, nH) * 3 + 1];

            sumG += 4 * ImgIn[indicePixel(x-2, y+1, nW, nH) * 3 + 1];
            sumG += 16 * ImgIn[indicePixel(x-1, y+1, nW, nH) * 3 + 1];
            sumG += 26 * ImgIn[indicePixel(x, y+1, nW, nH) * 3 + 1];
            sumG += 16 * ImgIn[indicePixel(x+1, y+1, nW, nH) * 3 + 1];
            sumG += 4 * ImgIn[indicePixel(x+2, y+1, nW, nH) * 3 + 1];

            sumG += 1 * ImgIn[indicePixel(x-2, y+2, nW, nH) * 3 + 1];
            sumG += 4 * ImgIn[indicePixel(x-1, y+2, nW, nH) * 3 + 1];
            sumG += 7 * ImgIn[indicePixel(x, y+2, nW, nH) * 3 + 1];
            sumG += 4 * ImgIn[indicePixel(x+1, y+2, nW, nH) * 3 + 1];
            sumG += 1 * ImgIn[indicePixel(x+2, y+2, nW, nH) * 3 + 1];

            sumB += 1 * ImgIn[indicePixel(x-2, y-2, nW, nH) * 3 + 2];
            sumB += 4 * ImgIn[indicePixel(x-1, y-2, nW, nH) * 3 + 2];
            sumB += 7 * ImgIn[indicePixel(x, y-2, nW, nH) * 3 + 2];
            sumB += 4 * ImgIn[indicePixel(x+1, y-2, nW, nH) * 3 + 2];
            sumB += 1 * ImgIn[indicePixel(x+2, y-2, nW, nH) * 3 + 2];

            sumB += 4 * ImgIn[indicePixel(x-2, y-1, nW, nH) * 3 + 2];
            sumB += 16 * ImgIn[indicePixel(x-1, y-1, nW, nH) * 3 + 2];
            sumB += 26 * ImgIn[indicePixel(x, y-1, nW, nH) * 3 + 2];
            sumB += 16 * ImgIn[indicePixel(x+1, y-1, nW, nH) * 3 + 2];
            sumB += 4 * ImgIn[indicePixel(x+2, y-1, nW, nH) * 3 + 2];

            sumB += 7 * ImgIn[indicePixel(x-2, y, nW, nH) * 3 + 2];
            sumB += 26 * ImgIn[indicePixel(x-1, y, nW, nH) * 3 + 2];
            sumB += 41 * ImgIn[indicePixel(x, y, nW, nH) * 3 + 2];
            sumB += 26 * ImgIn[indicePixel(x+1, y, nW, nH) * 3 + 2];
            sumB += 7 * ImgIn[indicePixel(x+2, y, nW, nH) * 3 + 2];

            sumB += 4 * ImgIn[indicePixel(x-2, y+1, nW, nH) * 3 + 2];
            sumB += 16 * ImgIn[indicePixel(x-1, y+1, nW, nH) * 3 + 2];
            sumB += 26 * ImgIn[indicePixel(x, y+1, nW, nH) * 3 + 2];
            sumB += 16 * ImgIn[indicePixel(x+1, y+1, nW, nH) * 3 + 2];
            sumB += 4 * ImgIn[indicePixel(x+2, y+1, nW, nH) * 3 + 2];

            sumB += 1 * ImgIn[indicePixel(x-2, y+2, nW, nH) * 3 + 2];
            sumB += 4 * ImgIn[indicePixel(x-1, y+2, nW, nH) * 3 + 2];
            sumB += 7 * ImgIn[indicePixel(x, y+2, nW, nH) * 3 + 2];
            sumB += 4 * ImgIn[indicePixel(x+1, y+2, nW, nH) * 3 + 2];
            sumB += 1 * ImgIn[indicePixel(x+2, y+2, nW, nH) * 3 + 2];

            ImgOut[indicePixel(x, y, nW, nH) * 3] = (OCTET)(sumR / 273);
            ImgOut[indicePixel(x, y, nW, nH) * 3 + 1] = (OCTET)(sumG / 273);
            ImgOut[indicePixel(x, y, nW, nH) * 3 + 2] = (OCTET)(sumB / 273);
        }
    }
}

void RGBToY(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut)  {
    for(int y=0; y<nH; y++) {
        for(int x=0; x<nW; x++) {
            int pos = 3*(indicePixel(x,y,nW,nH));
            ImgOut[indicePixel(x,y,nW,nH)] = (int)((ImgIn[pos] + ImgIn[pos+1] + ImgIn[pos+2])/3);
        }
    }
}

double EQM_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgIn2) {
    double somme = 0;
    for(int y=0; y<nH; y++) {
        for(int x=0; x<nW; x++) {
            somme += (ImgIn[indicePixel(x,y,nW,nH)]-ImgIn2[indicePixel(x,y,nW,nH)]) * (ImgIn[indicePixel(x,y,nW,nH)]-ImgIn2[indicePixel(x,y,nW,nH)]);
        }
    }
    return sqrt(somme/(nH*nW));
}

double EQM_ppm(int nW, int nH, OCTET* ImgIn, OCTET* ImgIn2) {
    double somme = 0;
    for(int y = 2; y < nH - 2; y++) {
        for(int x = 2; x < nW - 2; x++) {
            int redIndex = indicePixel(x, y, nW, nH) * 3;
            int greenIndex = indicePixel(x, y, nW, nH) * 3 + 1;
            int blueIndex = indicePixel(x, y, nW, nH) * 3 + 2;
            somme += (ImgIn[redIndex] - ImgIn2[redIndex]) * (ImgIn[redIndex] - ImgIn2[redIndex]);
            somme += (ImgIn[greenIndex] - ImgIn2[greenIndex]) * (ImgIn[greenIndex] - ImgIn2[greenIndex]);
            somme += (ImgIn[blueIndex] - ImgIn2[blueIndex]) * (ImgIn[blueIndex] - ImgIn2[blueIndex]);
        }
    }
    return sqrt(somme / ((nH - 4) * (nW - 4) * 3));
}

void RGBtoYCbCr(int nW, int nH, OCTET* ImgIn, OCTET* ImgOutY, OCTET* ImgOutCb, OCTET* ImgOutCr) {
    for(int y = 0; y < nH; y++) {
        for(int x = 0; x < nW; x++) {
            int pos = 3 * (y * nW + x);
            int r = ImgIn[pos];
            int g = ImgIn[pos + 1];
            int b = ImgIn[pos + 2];

            int yVal = (int)(0.299 * r + 0.587 * g + 0.114 * b);
            int cbVal = (int)(-0.169 * r - 0.331 * g + 0.5 * b + 128);
            int crVal = (int)(0.5 * r - 0.419 * g - 0.081 * b + 128);

            yVal = (yVal < 0) ? 0 : ((yVal > 255) ? 255 : yVal);
            cbVal = (cbVal < 0) ? 0 : ((cbVal > 255) ? 255 : cbVal);
            crVal = (crVal < 0) ? 0 : ((crVal > 255) ? 255 : crVal);

            ImgOutY[y * nW + x] = (OCTET)yVal;
            ImgOutCb[y * nW + x] = (OCTET)cbVal;
            ImgOutCr[y * nW + x] = (OCTET)crVal;
        }
    }
}

void YCbCrtoRGB(int nW, int nH, OCTET* ImgInY, OCTET* ImgInCb, OCTET* ImgInCr, OCTET* ImgOut) {
    for(int y = 0; y < nH; y++) {
        for(int x = 0; x < nW; x++) {
            int Y = ImgInY[indicePixel(x,y,nW,nH)];
            int Cb = ImgInCb[indicePixel(x,y,nW,nH)] - 128;
            int Cr = ImgInCr[indicePixel(x,y,nW,nH)] - 128;

            int R = Y + 1.402 * Cr;
            int G = Y - 0.344 * Cb - 0.714 * Cr;
            int B = Y + 1.772 * Cb;

            R = (R < 0) ? 0 : ((R > 255) ? 255 : R);
            G = (G < 0) ? 0 : ((G > 255) ? 255 : G);
            B = (B < 0) ? 0 : ((B > 255) ? 255 : B);

            int index = 3 * indicePixel(x,y,nW,nH);
            ImgOut[index] = (OCTET)R; // R a changer pour inversion
            ImgOut[index + 1] = (OCTET)G; // G ..
            ImgOut[index + 2] = (OCTET)B; // B ..
        }
    }
}

void modifY(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    int k;
    printf("k: ");
    scanf("%d", &k);

    for (int i=0; i<nW*nH; i++) {
        int val = ImgIn[i];
        val += k;
        if (val < 0) val = 0;
        if (val > 255) val = 255;
        ImgOut[i] = val;
    }
}

void ddp_pgm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    double nbOcc[256] = {0};
    double ddp[256] = {0};

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x, y, nW, nH)];
            nbOcc[valPixel]++;
        }
    }

    int totalPixels = nW * nH;
    for (int i = 0; i < 256; i++) {
        ddp[i] = nbOcc[i] / totalPixels;
    }

    ofstream file(nom_fichier, ios::out | ios::binary);
    for (int i = 0; i < 256; i++) {
        file << i << " "  << ddp[i] << endl;
    }
    file.close();

    printf("plot \"%s\" with lines\n", nom_fichier);
}


void ddp_ppm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    double nbOccR[256] = {0}; // Array for red channel
    double nbOccG[256] = {0}; // Array for green channel
    double nbOccB[256] = {0}; // Array for blue channel
    double ddpR[256] = {0};    // DDP for red channel
    double ddpG[256] = {0};    // DDP for green channel
    double ddpB[256] = {0};    // DDP for blue channel

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET* pixel = &ImgIn[3 * (indicePixel(x, y, nW, nH))];
            nbOccR[pixel[0]]++; // Red channel
            nbOccG[pixel[1]]++; // Green channel
            nbOccB[pixel[2]]++; // Blue channel
        }
    }

    int totalPixels = nW * nH;
    for (int i = 0; i < 256; i++) {
        ddpR[i] = nbOccR[i] / totalPixels; // Red channel DDP
        ddpG[i] = nbOccG[i] / totalPixels; // Green channel DDP
        ddpB[i] = nbOccB[i] / totalPixels; // Blue channel DDP
    }

    ofstream file(nom_fichier, ios::out | ios::binary);
    for (int i = 0; i < 256; i++) {
        file << i << " "  << ddpR[i] << " " << ddpG[i] << " " << ddpB[i] << endl; // Writing RGB DDP
    }
    file.close();

    printf("plot \"%s\" using 1:2 with lines lc \"red\" title \"Red\", \"%s\" using 1:3 with lines lc \"green\" title \"Green\", \"%s\" using 1:4 with lines lc \"blue\" title \"Blue\"\n", nom_fichier, nom_fichier, nom_fichier);

}


void repartition_pgm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    double nbOcc[256] = {0};
    double ddp[256] = {0};
    double F[256] = {0};

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x, y, nW, nH)];
            nbOcc[valPixel]++;
        }
    }

    int totalPixels = nW * nH;

    ddp[0] = nbOcc[0] / totalPixels;
    F[0] = ddp[0];
    for (int i = 1; i < 256; i++) {
        ddp[i] = nbOcc[i] / totalPixels;
        F[i] = F[i - 1] + ddp[i];
    }

    ofstream file(nom_fichier, ios::out | ios::binary);
    for (int i = 0; i < 256; i++) {
        file << i << " " << F[i] << endl;
    }
    file.close();
    printf("plot \"%s\" with lines\n", nom_fichier);
}

void repartition_ppm(int nW, int nH, OCTET* ImgIn, char nom_fichier[]) {
    double nbOccR[256] = {0};
    double nbOccG[256] = {0};
    double nbOccB[256] = {0};
    double ddpR[256] = {0};
    double ddpG[256] = {0};
    double ddpB[256] = {0};

    for (int y = 2; y < nH - 2; y++) {
        for (int x = 2; x < nW - 2; x++) {
            OCTET valRed = ImgIn[indicePixel(x, y, nW, nH) * 3];
            OCTET valGreen = ImgIn[indicePixel(x, y, nW, nH) * 3 + 1];
            OCTET valBlue = ImgIn[indicePixel(x, y, nW, nH) * 3 + 2];
            nbOccR[valRed]++;
            nbOccG[valGreen]++;
            nbOccB[valBlue]++;
        }
    }

    int totalPixels = (nW - 4) * (nH - 4);

    ddpR[0] = nbOccR[0] / totalPixels;
    ddpG[0] = nbOccG[0] / totalPixels;
    ddpB[0] = nbOccB[0] / totalPixels;
    for (int i = 1; i < 256; i++) {
        ddpR[i] = ddpR[i - 1] + nbOccR[i] / totalPixels;
        ddpG[i] = ddpG[i - 1] + nbOccG[i] / totalPixels;
        ddpB[i] = ddpB[i - 1] + nbOccB[i] / totalPixels;
    }

    ofstream file(nom_fichier, ios::out | ios::binary);
    for (int i = 0; i < 256; i++) {
        file << i << ' ' << ddpR[i] << ' ' << ddpG[i] << ' ' << ddpB[i] << endl;
    }
    file.close();

    printf("plot \"%s\" using 1:2 with lines lc \"red\" title \"Red\", \"%s\" using 1:3 with lines lc \"green\" title \"Green\", \"%s\" using 1:4 with lines lc \"blue\" title \"Blue\"\n", nom_fichier, nom_fichier, nom_fichier);

}

void egalisation_pgm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    double nbOcc[256] = {0};
    double ddp[256] = {0};
    double F[256] = {0};

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x, y, nW, nH)];
            nbOcc[valPixel]++;
        }
    }

    int totalPixels = nW * nH;

    ddp[0] = nbOcc[0] / totalPixels;
    F[0] = ddp[0];
    for (int i = 1; i < 256; i++) {
        ddp[i] = nbOcc[i] / totalPixels;
        F[i] = F[i - 1] + ddp[i];
    }

    for (int y = 0; y < nH; y++) {
        for (int x = 0; x < nW; x++) {
            OCTET valPixel = ImgIn[indicePixel(x, y, nW, nH)];
            ImgOut[indicePixel(x, y, nW, nH)] = floor(F[valPixel] * 255);
        }
    }
}


void egalisation_ppm(int nW, int nH, OCTET* ImgIn, OCTET* ImgOut) {
    double nbOcc[256] = {0};
    double ddp[256] = {0};
    double F[256] = {0};

    for (int y = 2; y < nH - 2; y++) {
        for (int x = 2; x < nW - 2; x++) {
            OCTET valRed = ImgIn[indicePixel(x, y, nW, nH) * 3];
            nbOcc[valRed]++;
        }
    }

    int totalPixels = (nW - 4) * (nH - 4);

    ddp[0] = nbOcc[0] / totalPixels;
    F[0] = ddp[0];
    for (int i = 1; i < 256; i++) {
        ddp[i] = nbOcc[i] / totalPixels;
        F[i] = F[i - 1] + ddp[i];
    }

    for (int y = 2; y < nH - 2; y++) {
        for (int x = 2; x < nW - 2; x++) {
            OCTET valRed = ImgIn[indicePixel(x, y, nW, nH) * 3];
            OCTET valGreen = ImgIn[indicePixel(x, y, nW, nH) * 3 + 1];
            OCTET valBlue = ImgIn[indicePixel(x, y, nW, nH) * 3 + 2];

            ImgOut[indicePixel(x, y, nW, nH) * 3] = floor(F[valRed] * 255);
            ImgOut[indicePixel(x, y, nW, nH) * 3 + 1] = floor(F[valGreen] * 255);
            ImgOut[indicePixel(x, y, nW, nH) * 3 + 2] = floor(F[valBlue] * 255);
        }
    }
}

